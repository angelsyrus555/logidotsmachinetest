//
//  HomeVC.swift
//  LogidotsMachineTest
//
//  Created by Angel F Syrus on 05/07/22.
//

import UIKit

class HomeVC: UIViewController {

    @IBOutlet weak var activityIndictor: UIActivityIndicatorView!
    
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var seasonCountCollection: UICollectionView!
    @IBOutlet weak var detailTable: UITableView!
    @IBOutlet weak var seasonLabel: UILabel!
    
    
    var seasonNoArr = [Int]()
    var seasonDetails = [Int : [HomeModel]]()
    var selectedSesaonDetails = [HomeModel]()
    var selectedIndex = "1"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.loadHomeData()
        self.viewLoadingProperties()
        
    }
    
 
}

extension HomeVC : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.seasonNoArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "topSeason", for: indexPath)as! TopSeasonCVC
        cell.seasonLabel.text = self.seasonNoArr[indexPath.row].description
        
        if Int(self.selectedIndex) == (indexPath.row + 1)
        {
            cell.outerView.backgroundColor = .darkGray
        }
        else
        {
            cell.outerView.backgroundColor = .lightGray
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? TopSeasonCVC else { return }
        
        self.selectedIndex = cell.seasonLabel.text ?? "0"
        self.seasonLabel.text = "Season  : " + self.selectedIndex
        let index = Int(self.selectedIndex)
        self.selectedSesaonDetails = self.seasonDetails[index!]!
        DispatchQueue.main.async {
            self.detailTable.reloadData()
            self.seasonCountCollection.reloadData()
        }
    }
    
   
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 70, height: 70)
    }
    
}

extension HomeVC : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.selectedSesaonDetails.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "seasonDetail")as! SeasonDetailTVC
        if !self.selectedSesaonDetails.isEmpty
        {
            cell.homeData = self.selectedSesaonDetails[indexPath.row]
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let nextVc = sb.instantiateViewController(withIdentifier: "PopUpVC")as! PopUpVC
        self.navigationController?.present(nextVc, animated: true, completion: nil)
    }
    
}

extension HomeVC
{
    func loadHomeData()
    {
        DispatchQueue.main.async {
            self.activityIndictor.startAnimating()
        }
        var request = URLRequest(url: URL(string: "https://api.tvmaze.com/shows/82/episodes")!)
        request.httpMethod = "GET"
        
        
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            DispatchQueue.main.async {
                self.activityIndictor.stopAnimating()
            }
                if let data = data {
                    
                    var userResponse : [HomeModel] = []
                    userResponse = try! JSONDecoder().decode([HomeModel].self, from: data)
                    let seasonArr = Array(Set(userResponse.map { ($0.season ?? 0) }))
                    self.seasonNoArr = seasonArr.sorted()
                    
                    self.seasonDetails = Dictionary(grouping: userResponse, by: { (element: HomeModel) in
                        return (element.season ?? 0)
                    })
                    self.selectedSesaonDetails = self.seasonDetails[1] ?? []
                    
                    DispatchQueue.main.async {
                        self.detailTable.reloadData()
                        self.seasonCountCollection.reloadData()
                    }
                }
        })
        
        task.resume()
    }
    
    func viewLoadingProperties()
    {
        let rightSwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture(gesture:)))
        rightSwipeGestureRecognizer.direction =  UISwipeGestureRecognizer.Direction.right
            self.detailTable.addGestureRecognizer(rightSwipeGestureRecognizer)

            // Add left swipe gesture recognizer
            let leftSwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture(gesture:)))
        leftSwipeGestureRecognizer.direction = UISwipeGestureRecognizer.Direction.left
            //sideMenuContainerView.addGestureRecognizer(rightSwipeGestureRecognizer)
            self.detailTable.addGestureRecognizer(leftSwipeGestureRecognizer)
    }
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {

        if let swipeGesture = gesture as? UISwipeGestureRecognizer {

            switch swipeGesture.direction {
            case .right:
                if self.selectedIndex != "1"
                {
                    var index = Int(self.selectedIndex)
                    index = index! - 1
                    self.selectedIndex = index!.description
                    print("right", index)

                    DispatchQueue.main.async {
                    self.detailTable.layer.rightAnimation(duration: 0.3)
                        self.seasonLabel.text = "Season  : " + self.selectedIndex
                        self.selectedSesaonDetails = self.seasonDetails[index!]!
                        self.detailTable.reloadData()
                        self.seasonCountCollection.reloadData()

            }
                }
            
            case .left:
                if self.selectedIndex != "7"
                {
                    var index = Int(self.selectedIndex)
                    index = index! + 1
                    self.selectedIndex = index!.description
                    print("left", index)

                    DispatchQueue.main.async {
                    self.detailTable.layer.leftAnimation(duration: 0.3)
                        self.seasonLabel.text = "Season  : " + self.selectedIndex
                        self.selectedSesaonDetails = self.seasonDetails[index!]!
                        self.detailTable.reloadData()
                        self.seasonCountCollection.reloadData()

            }
                }
            
            default:
                break
            }
        }
    }
    
 
}


// for transition animations
extension CALayer {
    
    func leftAnimation(duration:CFTimeInterval) {
        let animation = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        animation.duration = duration
        animation.type = CATransitionType.push
        animation.subtype = CATransitionSubtype.fromRight
        self.add(animation, forKey: CATransitionType.push.rawValue)
    }
    
    func rightAnimation(duration:CFTimeInterval) {
        let animation = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        animation.duration = duration
        animation.type = CATransitionType.push
        animation.subtype = CATransitionSubtype.fromLeft
        self.add(animation, forKey: CATransitionType.push.rawValue)
    }
}

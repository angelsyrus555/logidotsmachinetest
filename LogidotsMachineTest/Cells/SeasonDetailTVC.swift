//
//  SeasonDetailTVC.swift
//  LogidotsMachineTest
//
//  Created by Angel F Syrus on 05/07/22.
//

import UIKit
import Kingfisher

class SeasonDetailTVC: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var chapterLabel: UILabel!
    @IBOutlet weak var descriptionlabel: UILabel!
    @IBOutlet weak var chapterImage: UIImageView!
    
    var homeData : HomeModel?
    {
        didSet
        {
            self.titleLabel.text = " - " + ( homeData?.name ?? "")
            self.dateLabel.text = "Air date : " + (homeData?.airdate ?? "")
            self.chapterLabel.text = homeData?.number?.description
            self.descriptionlabel.text = homeData?.summary ?? ""
            let imagelink = homeData?.image?.medium
            if imagelink != nil
            {
                let url = URL(string: imagelink!)
                self.chapterImage.kf.indicatorType = .activity
                self.chapterImage.kf.setImage(with: url, placeholder: nil, options: .none, completionHandler: nil)
                
            }

        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
